'''task doesn't look like it worth solving by decorators, so i won't '''


some_nums = []
for i in range(60):
    some_nums.append(i)


def some_func(some_list):
    res = []
    for num in some_list:
        if num % 2 != 0:
            res.append(num*num)
    return res


if __name__ == '__main__':
    print(some_nums)
    print(some_func(some_nums))
    