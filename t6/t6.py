def fibonacci_generator():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


def even_numbers_only(generator):
    for number in generator:
        if number % 2 == 0:
            yield number


fibonacci_even = even_numbers_only(fibonacci_generator())


for i in range(10):
    print(next(fibonacci_even))
