def even_numbers_only(generator_func):
    '''this is decorator function theoretically usable for any generator function'''
    def wrapper():
        generator = generator_func()
        for number in generator:
            if number % 2 == 0:
                yield number
    return wrapper


@even_numbers_only
def fibonacci_generator():
    '''this is some kind of generator of fibonacci numbers'''
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


fibonacci_even = fibonacci_generator()

for i in range(10):
    print(next(fibonacci_even))
