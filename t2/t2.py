import time
import functools


def time_tracker(func):
    '''theoretically universal decorator for tracking funcion's run time.
    WARNING: this will not work as planned for recursive functions'''


    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper


@time_tracker
def my_func():
    for i in range(10):
        time.sleep(0.5)
        print(i)


if __name__ == '__main__':
    my_func()
    