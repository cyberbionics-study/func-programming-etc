import functools


def cache(func):
    """Keep a cache of previous function calls"""
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        cache_key = args + tuple(kwargs.items())
        if cache_key not in wrapper.cache:
            wrapper.cache[cache_key] = func(*args, **kwargs)
        return wrapper.cache[cache_key]
    wrapper.cache = dict()
    return wrapper


@cache
def fibonacci(num):
    '''generator of fibonacci numbers decorated by regular cache function'''
    if num < 2:
        return num
    return fibonacci(num - 1) + fibonacci(num - 2)


if __name__ == '__main__':
    print(fibonacci(25))
