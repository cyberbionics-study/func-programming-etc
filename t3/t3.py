def fibonacci(num):
    '''regular generator of fibonacci numbers'''
    if num < 2:
        return num
    return fibonacci(num - 1) + fibonacci(num - 2)


if __name__ == '__main__':
    print(fibonacci(25))
