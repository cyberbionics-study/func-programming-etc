import functools


@functools.lru_cache(maxsize=16)
def fibonacci(num):
    '''generator of fibonacci numbers decorated by lru cache function with cache size of 16'''

    if num < 2:
        return num
    return fibonacci(num - 1) + fibonacci(num - 2)


if __name__ == '__main__':
    print(fibonacci(25))
