def mult(x, y):
    '''multiplying function'''
    return x * y


def curry_mult(x):
    '''curried multiplying function'''
    def inner(y):
        return x * y
    return inner


multiply_by_2 = curry_mult(2)  # here we partly use curry func for 1 arg
result1 = multiply_by_2(5)
print(result1)

result2 = curry_mult(3)(4)  # here we partly use curry func for 2 args
print(result2)
