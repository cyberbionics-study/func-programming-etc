'''here I use module timeit for calculating average working time of each t3's functions'''
import timeit
from t3 import t3, t3_1, t3_2, t3_3


time_taken_raw = timeit.timeit(lambda: t3.fibonacci(25), number=10)
print(f"Average time for executing function with n={25} and no cache: {time_taken_raw / 10:.6f} seconds")

time_taken_cache = timeit.timeit(lambda: t3_1.fibonacci(25), number=10)
print(f"Average time for executing function with n={25} using cache: {time_taken_cache / 10:.6f} seconds")

time_taken_lru_10 = timeit.timeit(lambda: t3_2.fibonacci(25), number=10)
print(f"Average time for executing function with n={25} using lru cache (maxsize=10): "
      f"{time_taken_lru_10 / 10:.6f} seconds")

time_taken_lru_16 = timeit.timeit(lambda: t3_3.fibonacci(25), number=10)
print(f"Average time for executing function with n={25} using lru cache (maxsize=16): "
      f"{time_taken_lru_16 / 10:.6f} seconds")
